package com.example.spring.oauth.provider;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

@Configuration
@EnableAuthorizationServer
public class AuthServerConfig extends AuthorizationServerConfigurerAdapter {

    private final DataSource ds;

    private final AuthenticationManager authMgr;

    private final UserDetailsService usrSvc;

    private final String keyValue;

    public AuthServerConfig(final DataSource ds, final AuthenticationManager authMgr,
            final UserDetailsService usrSvc,
            @Value("${oauth.provider.jwt.key-value:}") final String keyValue) {
        this.ds = ds;
        this.authMgr = authMgr;
        this.usrSvc = usrSvc;
        this.keyValue = keyValue;
    }

    @Bean("clientPasswordEncoder")
    PasswordEncoder clientPasswordEncoder() {
        return new BCryptPasswordEncoder(8);
    }

    @Bean
    JwtAccessTokenConverter jwtAccessTokenConverter() {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        converter.setSigningKey(keyValue);
        return converter;
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer cfg) throws Exception {

        // Enable /oauth/token_key URL used by resource server to validate JWT tokens
        cfg.tokenKeyAccess("permitAll");

        // Enable /oauth/check_token URL
        cfg.checkTokenAccess("permitAll");

        // BCryptPasswordEncoder(8) is used for oauth_client_details.user_secret
        cfg.passwordEncoder(clientPasswordEncoder());
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        // OAuthクライアント設定の読み込み元を指定。
        // 今回はDBに定義させたいのでDataSourceを指定する。
        // どうやら、テーブルの型はSpring Security OAuth2の仕様で決まっているらしい。
        // （いろいろ設定すれば変えられそうな気もするが）
        // デフォルトでは「oauth_client_details」というテーブル名で作らないとならないらしい。
        // 詳細はresources/schema.sqlの定義を参照。
        clients.jdbc(ds);
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {

        endpoints.accessTokenConverter(jwtAccessTokenConverter());
        endpoints.authenticationManager(authMgr);
        endpoints.userDetailsService(usrSvc);
    }

}
