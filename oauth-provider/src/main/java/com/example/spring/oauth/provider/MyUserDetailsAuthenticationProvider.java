package com.example.spring.oauth.provider;

import java.util.List;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class MyUserDetailsAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

    // 認証では使用しないため、値は何でもよい(nullや空文字列はNGらしい)
    private static final String DUMMY_PASSWORD = "DUMMY_PASSWORD";

    // 実装しないといけないのでとりあえずOverrideするが必要ないので処理は書かない
    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails,
            UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {}

    @Override
    protected UserDetails retrieveUser(String username,
            UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {

        String password = (String) authentication.getCredentials();

        // ユーザ名とパスワードをチェック
        boolean isValid = isValidUserIdAndPassword(username, password);
        if (!isValid) {
            throw new UsernameNotFoundException(username);
        }

        // UserDetailsの実装(User)を生成し戻り値とする
        return new User(username, DUMMY_PASSWORD, getAuthority(username));
    }

    private boolean isValidUserIdAndPassword(String username, String password) {
        if ("user".equals(username) && "user@123".equals(password)) {
            return true;
        }
        if ("admin".equals(username) && "admin@123".equals(password)) {
            return true;
        }
        return false;
    }

    private List<GrantedAuthority> getAuthority(String username) {
        if ("user".equals(username)) {
            return AuthorityUtils.createAuthorityList("AUTHORIZED_PETSTORE_USER");
        }
        if ("admin".equals(username)) {
            return AuthorityUtils.createAuthorityList("AUTHORIZED_PETSTORE_USER",
                    "AUTHORIZED_PETSTORE_ADMIN");
        }
        return AuthorityUtils.createAuthorityList();
    }

}
