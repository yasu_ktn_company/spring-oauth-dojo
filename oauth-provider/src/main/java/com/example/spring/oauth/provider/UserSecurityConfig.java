package com.example.spring.oauth.provider;

import javax.sql.DataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import lombok.RequiredArgsConstructor;

@Configuration
@RequiredArgsConstructor
public class UserSecurityConfig extends WebSecurityConfigurerAdapter {

    private final DataSource ds;

    @Override
    @Bean(BeanIds.USER_DETAILS_SERVICE)
    public UserDetailsService userDetailsServiceBean() throws Exception {
        return super.userDetailsServiceBean();
    }

    @Override
    @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean("userPasswordEncoder")
    PasswordEncoder userPasswordEncoder() {
        return new BCryptPasswordEncoder(4);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        // --------
        // 下記はSpring Securityでデフォルト用意されているJDBD用の「UserDetailsAuthenticationProvider」
        // を使って認証を実装する例。
        // --------

        // BCryptPasswordEncoder(4) is used for users.password column
        // JdbcUserDetailsManagerConfigurer<AuthenticationManagerBuilder> cfg =
        // auth.jdbcAuthentication().passwordEncoder(userPasswordEncoder()).dataSource(ds);

        // cfg.getUserDetailsService().setEnableGroups(true);
        // cfg.getUserDetailsService().setEnableAuthorities(false);
        // --------


        // --------
        // 独自の認証機能を実装する場合は「UserDetailsAuthenticationProvider」を実装する。
        // この辺の使用はSpring Securiryなのでユーザーとパスワードで認証するにはどうしたらいいか
        // ネットで検索すると記事が見つかるはず。
        // --------
        auth.authenticationProvider(new MyUserDetailsAuthenticationProvider());
    }

}
